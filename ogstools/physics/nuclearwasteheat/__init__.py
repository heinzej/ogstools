# Author: Florian Zill (Helmholtz Centre for Environmental Research GmbH - UFZ)
# Co-Author: Christian B. Silbermann (TU Bergakademie Freiberg)
"Provide proxy models to calculate heat generated by nuclear waste."

from ._unitsetup import units
from .defaults import (
    repo_2020,
    repo_2020_conservative,
    repo_be_ha_2016,
    waste_types,
)

__all__ = [
    "units",
    "repo_2020",
    "repo_2020_conservative",
    "repo_be_ha_2016",
    "waste_types",
]
